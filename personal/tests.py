from django.test import TestCase, RequestFactory
from personal.views import index, contact


class TestPersonal(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_home(self):
        request = self.factory.get('/')
        response = index(request)
        self.assertEqual(response.status_code, 200)

    def test_contact(self):
        request = self.factory.get('/contact')
        response = contact(request)
        self.assertEqual(response.status_code, 200)
