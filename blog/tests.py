from django.test import TestCase
from django.urls import reverse
from blog.models import Post
from django.utils import timezone


class TestBlog(TestCase):
    def setUpTestData():
        Post.objects.create(title="test", body="test", date=timezone.now())

    def setUp(self):
        self.post = Post.objects.get(title="test")

    def test_posts_str(self):
        self.assertEqual(self.post.__str__(), "test")

    # Test to make sure all the proper database feilds are being setup
    def test_title_label(self):
        field_label = self.post._meta.get_field("title").verbose_name
        self.assertEqual(field_label, "title")

    def test_body_label(self):
        field_label = self.post._meta.get_field("body").verbose_name
        self.assertEqual(field_label, "body")

    def test_date_label(self):
        field_label = self.post._meta.get_field("date").verbose_name
        self.assertEqual(field_label, "date")

    def test_title_max_length(self):
        max_length = self.post._meta.get_field("title").max_length
        self.assertEqual(max_length, 100)

    def test_blog_url(self):
        resp = self.client.get("/blog/")
        self.assertEqual(resp.status_code, 200)

    def test_blog_url_name(self):
        resp = self.client.get(reverse("post-list"))
        self.assertEqual(resp.status_code, 200)

    def test_post_uses_correct_template_and_exists(self):
        resp = self.client.get("/blog/{}".format(self.post.id))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'blog/post.html')
